package main

import (
	"fmt"
	"time"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type Test struct {
	media.IFastoCloudNodeClientHandler
}

func (test *Test) OnClientStateChanged(cl *media.FastoCloudNodeClient, status gofastocloud_base.ConnectionStatus) {
	fmt.Printf("OnClientStateChanged: %d\n", status)
	if status == gofastocloud_base.ACTIVE {
		// cl.WebRTCInitStream(media.NewWebRTCInitRequest("608017795ef762439cabcf3a", "test"))
		// cl.ProbeOutStream(media.NewProbeOutStreamRequest(0, "udp://fastotv.com:10000"))
		/*extensions := []string{"m3u8"}
		cl.ScanFolder(media.NewScanFolderRequest("/home/fastocloud/streamer", extensions))*/
	}
}

func (test *Test) OnStreamStatisticReceived(client *media.FastoCloudNodeClient, statistic media.StreamStatisticInfo) {
	fmt.Printf("OnStreamStatisticReceived: %s\n", statistic)
}

func (test *Test) OnStreamSourcesChanged(client *media.FastoCloudNodeClient, source media.ChangedSourcesInfo) {
	fmt.Printf("OnStreamSourcesChanged: %s\n", source)
}

func (test *Test) OnStreamMlNotification(client *media.FastoCloudNodeClient, notify media.MlNotificationInfo) {
	fmt.Printf("OnStreamMlNotification: %s\n", notify)
}

func (test *Test) OnServiceStatisticReceived(client *media.FastoCloudNodeClient, statistic media.ServiceStatisticInfo) {
	fmt.Printf("OnServiceStatisticReceived: %s\n", statistic)
}

func (test *Test) OnCDNStatisticReceived(client *media.FastoCloudNodeClient, statistic media.CDNStatisticInfo) {
	fmt.Printf("OnCDNStatisticReceived: %s\n", statistic)
}

func (test *Test) OnStreamResultReady(client *media.FastoCloudNodeClient, result media.ResultStreamInfo) {
	fmt.Printf("OnStreamResultReady: %s\n", result)
}

func (test *Test) OnWebRTCOutInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutInitInfo) {
	fmt.Printf("OnWebRTCOutInitReceived: %s\n", web)
}

func (test *Test) OnWebRTCOutDeInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutDeInitInfo) {
	fmt.Printf("OnWebRTCOutDeinitReceived: %s\n", web)
}

func (test *Test) OnWebRTCOutSdpReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutSdpInfo) {
	fmt.Printf("OnWebRTCOutSdpReceived: %s\n", web)
}

func (test *Test) OnWebRTCOutIceReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutIceInfo) {
	fmt.Printf("OnWebRTCOutIceReceived: %s\n", web)
}

func (test *Test) OnWebRTCInInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCInInitInfo) {
	fmt.Printf("OnWebRTCInInitReceived: %s\n", web)
}

func (test *Test) OnWebRTCInDeInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCInDeInitInfo) {
	fmt.Printf("OnWebRTCInDeinitReceived: %s\n", web)
}

func (test *Test) OnWebRTCInSdpReceived(client *media.FastoCloudNodeClient, web media.WebRTCInSdpInfo) {
	fmt.Printf("OnWebRTCInSdpReceived: %s\n", web)
}

func (test *Test) OnWebRTCInIceReceived(client *media.FastoCloudNodeClient, web media.WebRTCInIceInfo) {
	fmt.Printf("OnWebRTCInIceReceived: %s\n", web)
}

func (test *Test) OnQuitStatusStream(client *media.FastoCloudNodeClient, status media.QuitStatusInfo) {
	fmt.Printf("OnQuitStatusStream: %s\n", status)
}

func (test *Test) OnPingReceived(client *media.FastoCloudNodeClient, ping media.PingInfo) {
	fmt.Printf("OnPingReceived: %s\n", ping)
}

func main() {
	test := Test{}
	client := media.NewFastoCloudNodeClient(gofastogt.HostAndPort{Host: "fastotv.com", Port: 6317}, &test)
	err := client.Connect()
	if err != nil {
		return
	}

	_, err = client.ActivateWithCallback(media.NewActivateRequest("006f68a6d700005eb12ee30000aa54e02c0170455b1af40a70901c4ef60ba0eea82cf50d2044e891c1527e5a9fafd7400"), nil)
	if err != nil {
		return
	}

	ch := make(chan []byte)
	eCh := make(chan error)

	// Start a goroutine to read from our net connection
	go func(ch chan []byte, eCh chan error) {
		for {
			data, err := client.ReadCommand()
			if err != nil {
				eCh <- err
				return
			}
			ch <- data
		}
	}(ch, eCh)

	ticker := time.Tick(time.Second)
	for {
		select {
		case data := <-ch:
			client.ProcessCommand(data)
			break
		case err := <-eCh:
			print(err)
			break
		case <-ticker:
			break
		}
	}

	client.Disconnect()
}
