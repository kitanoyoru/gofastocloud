package load_balance

import "gitlab.com/fastogt/gofastogt/gofastogt"

type OnlineUsers struct {
	Daemon      int `json:"daemon"`
	Subscribers int `json:"subscribers"`
}

type ServiceStatisticInfo struct {
	Cpu           float64               `json:"cpu"`
	Gpu           float64               `json:"gpu"`
	LoadAverage   string                `json:"load_average"`
	MemoryTotal   int64                 `json:"memory_total"`
	MemoryFree    int64                 `json:"memory_free"`
	HddTotal      int64                 `json:"hdd_total"`
	HddFree       int64                 `json:"hdd_free"`
	BandwidthIn   int                   `json:"bandwidth_in"`
	BandwidthOut  int                   `json:"bandwidth_out"`
	Uptime        gofastogt.UtcTimeMsec `json:"uptime"`
	Timestamp     int64                 `json:"timestamp"`
	TotalBytesIn  int64                 `json:"total_bytes_in"`
	TotalBytesOut int64                 `json:"total_bytes_out"`
	OnlineUsers   OnlineUsers           `json:"online_users"`
}

type PingInfo struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

type MessageType int

const (
	TEXT MessageType = iota
	HYPERLINK
)

func (s MessageType) IsValid() bool {
	switch s {
	case TEXT, HYPERLINK:
		return true
	}
	return false
}
