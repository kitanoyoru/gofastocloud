package media

type VideoFlip uint8

func VideoFlipFromRotation(rotate int) VideoFlip {
	if rotate == 90 {
		return 1
	} else if rotate == 180 {
		return 2
	} else if rotate == -90 {
		return 3
	}
	return 0
}
