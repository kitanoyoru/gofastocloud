package media

import "gitlab.com/fastogt/gofastogt/gofastogt"

type InferLayer struct {
	Name string        `json:"name"`
	Size uint64        `json:"size"`
	Type InferDataType `json:"type"`
	Data []float32     `json:"data"`
}

type LabelInfo struct {
	Label       string  `json:"label"`
	LID         uint32  `json:"id"`
	CID         uint32  `json:"class_id"`
	Probability float32 `json:"probability"`
}

type ImageBox struct {
	Sender     string                 `json:"sender"`
	Timestamp  gofastogt.DurationMsec `json:"timestamp"`
	UID        int32                  `json:"unique_component_id"`
	CID        int32                  `json:"class_id"`
	OID        uint64                 `json:"object_id"`
	Confidence float32                `json:"confidence"`
	Left       int32                  `json:"left"`
	Top        int32                  `json:"top"`
	Width      int32                  `json:"width"`
	Height     int32                  `json:"height"`
	Layers     []InferLayer           `json:"layers"`
	Labels     []LabelInfo            `json:"labels"`
}

type MlNotificationInfo struct {
	Id     StreamId   `json:"id"`
	Images []ImageBox `json:"images"`
}
