package media

import (
	"fmt"
	"strings"
)

type WebRTCSdpType int

const (
	OFFER    WebRTCSdpType = 1
	PRANSWER WebRTCSdpType = 2
	ANSWER   WebRTCSdpType = 3
	ROLLBACK WebRTCSdpType = 4
)

func ParseWebRTCSdpType(sdp string) (WebRTCSdpType, error) {
	switch strings.ToLower(sdp) {
	case "offer":
		return OFFER, nil
	case "pranswer":
		return PRANSWER, nil
	case "answer":
		return ANSWER, nil
	case "rollback":
		return ROLLBACK, nil
	}

	var l WebRTCSdpType
	return l, fmt.Errorf("not a valid WebRTCSdpType: %s", sdp)
}

// { "id": "6082ff315ef762285233970e", "init": { "connection_id": "test" } }

type WebRTCSubInitInfo struct {
	ConnectionId string      `json:"connection_id"`
	WebRTC       *WebRTCProp `json:"webrtc,omitempty"`
}

type WebRTCOutInitInfo struct {
	Id   StreamId          `json:"id"`
	Init WebRTCSubInitInfo `json:"init"`
}

type WebRTCInInitInfo struct {
	Id   StreamId          `json:"id"`
	Init WebRTCSubInitInfo `json:"init"`
}

// { "id": "6082ff315ef762285233970e", "description": { "connection_id": "test", "type": 1, "sdp": "some" } }

type WebRTCSubSdpInfo struct {
	ConnectionId string `json:"connection_id"`
	Type         string `json:"type"`
	Sdp          string `json:"sdp"`
}

type WebRTCOutSdpInfo struct {
	Id          StreamId         `json:"id"`
	Description WebRTCSubSdpInfo `json:"description"`
}

type WebRTCInSdpInfo struct {
	Id          StreamId         `json:"id"`
	Description WebRTCSubSdpInfo `json:"description"`
}

// { "id": "6082ff315ef762285233970e", "ice": { "connection_id": "test", "sdpMLineIndex": 0, "candidate": "candidate" } }

type WebRTCSubIceInfo struct {
	ConnectionId  string `json:"connection_id"`
	Candidate     string `json:"candidate"`
	SdpMLineIndex int    `json:"sdpMLineIndex"`
	SdpMid        string `json:"sdpMid"`
}

type WebRTCOutIceInfo struct {
	Id  StreamId         `json:"id"`
	Ice WebRTCSubIceInfo `json:"ice"`
}

type WebRTCInIceInfo struct {
	Id  StreamId         `json:"id"`
	Ice WebRTCSubIceInfo `json:"ice"`
}

// { "id": "6082ff315ef762285233970e", "deinit": { "connection_id": "test" } }

type WebRTCSubDeInitInfo struct {
	ConnectionId string `json:"connection_id"`
}

type WebRTCOutDeInitInfo struct {
	Id     StreamId            `json:"id"`
	DeInit WebRTCSubDeInitInfo `json:"deinit"`
}

type WebRTCInDeInitInfo struct {
	Id     StreamId            `json:"id"`
	DeInit WebRTCSubDeInitInfo `json:"deinit"`
}
