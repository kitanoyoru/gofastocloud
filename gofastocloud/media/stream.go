package media

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type streamStatisticInfo struct {
	Id            StreamId                    `json:"id"`
	Type          StreamType                  `json:"type"`
	Cpu           float64                     `json:"cpu"`
	LoopStartTime gofastogt.UtcTimeMsec       `json:"loop_start_time"`
	Rss           int32                       `json:"rss"`
	Status        StreamStatus                `json:"status"`
	Restarts      int64                       `json:"restarts"`
	StartTime     gofastogt.UtcTimeMsec       `json:"start_time"`
	Timestamp     gofastogt.UtcTimeMsec       `json:"timestamp"`
	IdleTime      int64                       `json:"idle_time"`
	InputStreams  []InputStreamStatisticInfo  `json:"input_streams"`
	OutputStreams []OutputStreamStatisticInfo `json:"output_streams"`
}

type StreamStatisticInfo struct {
	streamStatisticInfo

	// calc
	Quality float64 `json:"quality"`
}

func (result *StreamStatisticInfo) UnmarshalJSON(data []byte) error {
	var stat streamStatisticInfo
	err := json.Unmarshal(data, &stat)
	if err != nil {
		return err
	}

	*result = StreamStatisticInfo{stat, stat.CalcQuality()}
	return nil
}

func (stat *streamStatisticInfo) CalcQuality() float64 {
	workTime := stat.Timestamp - stat.StartTime
	if workTime == 0 {
		return 100.0
	}
	div := float64(stat.IdleTime) / float64(workTime)
	quality := 100.0 - 100.0*div
	return quality
}

type QuitStatusInfo struct {
	Id         StreamId   `json:"id"`
	ExitStatus ExitStatus `json:"exit_status"`
	Signal     int        `json:"signal"`
}

type ResultStreamInfo struct {
	Id      StreamId `json:"id"`
	Payload string   `json:"payload"`
}

type ChangedSourcesInfo struct {
	Id  StreamId `json:"id"`
	Uri InputUrl `json:"url"`
}
