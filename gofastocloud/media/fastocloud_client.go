package media

import (
	"encoding/json"
	"net/url"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// FIXME: UnmarshalJSON
type ServerStaticInfo struct {
	Project         string                            `json:"project"`
	Version         string                            `json:"version"`
	ExpirationTime  gofastogt.UtcTimeMsec             `json:"expiration_time"`
	OperationSystem gofastocloud_base.OperationSystem `json:"os"`

	HttpHost string `json:"hls_host"`
	VodsHost string `json:"vods_host"`
	CodsHost string `json:"cods_host"`

	HlsDir        string `json:"hls_dir"`
	VodsDir       string `json:"vods_dir"`
	CodsDir       string `json:"cods_dir"`
	TimeshiftsDir string `json:"timeshifts_dir"`
	FeedbackDir   string `json:"feedback_dir"`
	ProxyDir      string `json:"proxy_dir"`
	DataDir       string `json:"data_dir"`
}

const (
	FASTOCLOUD_COM    string = "fastocloud"
	FASTOCLOUD_PRO    string = "fastocloud_pro"
	FASTOCLOUD_PRO_ML string = "fastocloud_pro_ml"
)

type FullServerInfo struct {
	ServerStaticInfo
	Runtime ServiceStatisticInfo
}

type FastoCloudClient struct {
	*gofastocloud_base.Client

	info *FullServerInfo
}

func NewFastoCloudClient(host gofastogt.HostAndPort, observer gofastocloud_base.IClientObserver) *FastoCloudClient {
	client := gofastocloud_base.NewGzipClient(host, observer)
	p := FastoCloudClient{client, nil}
	return &p
}

func (client *FastoCloudClient) GetHttpHost() *url.URL {
	if client.info != nil {
		result, err := url.Parse(client.info.HttpHost)
		if err != nil {
			return nil
		}
		return result
	}
	return nil
}

func (client *FastoCloudClient) GetVodsHost() *url.URL {
	if client.info != nil {
		result, err := url.Parse(client.info.VodsHost)
		if err != nil {
			return nil
		}
		return result
	}
	return nil
}

func (client *FastoCloudClient) GetCodsHost() *url.URL {
	if client.info != nil {
		result, err := url.Parse(client.info.CodsHost)
		if err != nil {
			return nil
		}
		return result
	}
	return nil
}

func (client *FastoCloudClient) GetProject() *string {
	if client.info != nil {
		return &client.info.Project
	}
	return nil
}

func (client *FastoCloudClient) GetVersion() *string {
	if client.info != nil {
		return &client.info.Version
	}
	return nil
}

func (client *FastoCloudClient) GetExpirationTime() *gofastogt.UtcTimeMsec {
	if client.info != nil {
		return &client.info.ExpirationTime
	}
	return nil
}

func (client *FastoCloudClient) GetRuntimeStats() *ServiceStatisticInfo {
	if client.info != nil {
		return &client.info.Runtime
	}
	return nil
}

func (client *FastoCloudClient) GetOS() *gofastocloud_base.OperationSystem {
	if client.info != nil {
		return &client.info.OperationSystem
	}
	return nil
}

func (client *FastoCloudClient) IsPro() bool {
	if !client.IsActive() {
		return false
	}
	return client.info.IsPro()
}

func (info *FullServerInfo) IsPro() bool {
	return (info.Project == FASTOCLOUD_PRO || info.Project == FASTOCLOUD_PRO_ML)
}

func (client *FastoCloudClient) ActivateWithCallback(cid uint64, request *ActivateRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) PingWithCallback(cid uint64, request *PingRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) pong(cid *gofastocloud_base.RPCId) error {
	if !client.IsActive() {
		return ErrNotActive
	}

	pong := NewPingResponse()
	res, err := pong.toResponse(cid)
	if err != nil {
		return err
	}
	return client.SendResponseRaw(res)
}

func (client *FastoCloudClient) StopServiceWithCallback(cid uint64, request *StopServiceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) GetLogServiceWithCallback(cid uint64, request *GetLogServiceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) ProbeInStreamWithCallback(cid uint64, request *ProbeInStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) GetHardwareHashRequestWithCallback(cid uint64, request *HardwareHashRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) ProbeOutStreamWithCallback(cid uint64, request *ProbeOutStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) MountS3BucketWithCallback(cid uint64, request *MountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) UnMountS3BucketWithCallback(cid uint64, request *UnMountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) ScanS3BucketsWithCallback(cid uint64, request *ScanS3BucketsRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) ScanFolderWithCallback(cid uint64, request *ScanFolderRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) CleanStreamWithCallback(cid uint64, request *CleanStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

// streams section

func (client *FastoCloudClient) StartStreamWithCallback(cid uint64, request *StartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) StopStreamWithCallback(cid uint64, request *StopStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) RestartStreamWithCallback(cid uint64, request *RestartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) GetLogStreamWithCallback(cid uint64, request *GetLogStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) GetPipelineStreamWithCallback(cid uint64, request *GetPipelineStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) GetConfigJsonStreamWithCallback(cid uint64, request *GetConfigJsonStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) ChangeInputStreamWithCallback(cid uint64, request *ChangeInputStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCOutInitStreamWithCallback(cid uint64, request *WebRTCOutInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCOutDeinitStreamWithCallback(cid uint64, request *WebRTCOutDeInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCOutSDPStreamWithCallback(cid uint64, request *WebRTCOutSdpRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCOutICEStreamWithCallback(cid uint64, request *WebRTCOutIceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCInInitStreamWithCallback(cid uint64, request *WebRTCInInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCInDeinitStreamWithCallback(cid uint64, request *WebRTCInDeInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCInSDPStreamWithCallback(cid uint64, request *WebRTCInSdpRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) WebRTCInICEStreamWithCallback(cid uint64, request *WebRTCInIceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) InjectMasterInputUrlWithCallback(cid uint64, request *InjectMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

func (client *FastoCloudClient) RemoveMasterInputUrlWithCallback(cid uint64, request *RemoveMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if !client.IsActive() {
		return nil, ErrNotActive
	}

	if !client.IsPro() {
		return nil, ErrNotPro
	}

	req, err := request.toRequest(cid)
	if err != nil {
		return nil, err
	}
	return client.SendRequestWithCallback(req, callback)
}

// process

func (client *FastoCloudClient) ProcessCommand(data []byte) {
	if data == nil {
		return
	}

	req, resp := client.DecodeResponseOrRequest(data)
	if req != nil {
		if req.Method == kClientPingCommand {
			_ = client.pong(req.Id)
		} else if req.Method == kStatisticServiceBroadcastCommand {
			var statistic ServiceStatisticInfo
			if err := json.Unmarshal(*req.Params, &statistic); err == nil {
				client.info.Runtime = statistic
			}
		} else if req.Method == kStatisticCDNBroadcastCommand {

		}

		if client.Observer != nil {
			client.Observer.ProcessRequest(req)
		}
	} else if resp != nil {
		savedReq, callback := client.PopRequest(*resp.Id)
		if callback != nil {
			callback(savedReq, resp)
		}
		if savedReq != nil {
			if savedReq.Method == kActivateKeyCommand && resp.IsMessage() {
				client.SetStatus(gofastocloud_base.ACTIVE)
				var stat FullServerInfo
				err := json.Unmarshal(*resp.Result, &stat)
				if err == nil {
					client.setStaticFields(&stat)
				} else {
					client.setStaticFields(nil)
				}
			}
		}

		if client.Observer != nil {
			client.Observer.ProcessResponse(savedReq, resp)
		}
	}
}

func (client *FastoCloudClient) Reset() {
	client.setStaticFields(nil)
}

func (client *FastoCloudClient) setStaticFields(info *FullServerInfo) {
	client.info = info
}
