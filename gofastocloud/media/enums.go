package media

type BackgroundColor int
type AlphaMethodType int
type BackgroundEffectType int
type OverlayUrlType int
type StreamType int
type StreamStatus int
type StreamLogLevel int
type VodType int
type HlsSinkType int
type HlsType int
type RtmpSinkType int
type RtmpSrcType int
type QualityPrefer int
type GPUModel int
type AudioEffectType int
type SrtMode int
type UserAgent int
type ExitStatus int
type InferDataType int

const (
	FLOAT InferDataType = iota
	HALF
	INT8
	INT32
)

const (
	EXIT_SUCCESS ExitStatus = iota
	EXIT_FAILURE
)

func (e ExitStatus) IsValid() bool {
	switch e {
	case EXIT_SUCCESS, EXIT_FAILURE:
		return true
	}
	return false
}

const (
	NVIDIA_A100 GPUModel = iota
	NVIDIA_A10
	NVIDIA_T4
	NVIDIA_V100
)

const (
	DENOISER AudioEffectType = iota
	DEREVERB
	DEREVERB_DENOISER
)

const (
	CHECKER BackgroundColor = iota
	BLACK
	WHITE
	TRANSPARENT
)

const (
	SET AlphaMethodType = iota
	GREEN
	BLUE
	CUSTOM
)

const (
	BLUR BackgroundEffectType = iota
	IMAGE
	COLOR
)

const (
	URL OverlayUrlType = iota
	WPE
	CEF
)

const (
	STREAM_TYPE_PROXY StreamType = iota
	STREAM_TYPE_VOD_PROXY
	STREAM_TYPE_RELAY
	STREAM_TYPE_ENCODE
	STREAM_TYPE_TIMESHIFT_PLAYER
	STREAM_TYPE_TIMESHIFT_RECORDER
	STREAM_TYPE_CATCHUP
	STREAM_TYPE_TEST_LIFE
	STREAM_TYPE_VOD_RELAY
	STREAM_TYPE_VOD_ENCODE
	STREAM_TYPE_COD_RELAY
	STREAM_TYPE_COD_ENCODE
	STREAM_TYPE_EVENT
	STREAM_TYPE_CV_DATA
	STREAM_TYPE_CHANGER_RELAY
	STREAM_TYPE_CHANGER_ENCODE
	STREAM_TYPE_LITE
)

func (s StreamType) IsValid() bool {
	switch s {
	case STREAM_TYPE_PROXY, STREAM_TYPE_VOD_PROXY, STREAM_TYPE_RELAY, STREAM_TYPE_ENCODE, STREAM_TYPE_TIMESHIFT_PLAYER, STREAM_TYPE_TIMESHIFT_RECORDER,
		STREAM_TYPE_CATCHUP, STREAM_TYPE_TEST_LIFE, STREAM_TYPE_VOD_RELAY, STREAM_TYPE_VOD_ENCODE, STREAM_TYPE_COD_RELAY, STREAM_TYPE_COD_ENCODE,
		STREAM_TYPE_EVENT, STREAM_TYPE_CV_DATA, STREAM_TYPE_CHANGER_RELAY, STREAM_TYPE_CHANGER_ENCODE, STREAM_TYPE_LITE:
		return true
	}
	return false
}

func IsVodStreamType(st StreamType) bool {
	return st == STREAM_TYPE_VOD_ENCODE || st == STREAM_TYPE_VOD_PROXY || st == STREAM_TYPE_VOD_RELAY
}

const (
	NEW StreamStatus = iota
	INIT
	STARTED
	READY
	PLAYING
	FROZEN
	WAITING
)

const (
	VODS VodType = iota
	SERIES
)

const (
	LOG_LEVEL_EMERG StreamLogLevel = iota
	LOG_LEVEL_ALERT
	LOG_LEVEL_CRIT
	LOG_LEVEL_ERR
	LOG_LEVEL_WARNING
	LOG_LEVEL_NOTICE
	LOG_LEVEL_INFO
	LOG_LEVEL_DEBUG
)

const (
	HLS_PULL HlsType = iota
	HLS_PUSH
)

func (s HlsType) IsValid() bool {
	switch s {
	case HLS_PULL, HLS_PUSH:
		return true
	}
	return false
}

const (
	HLSSINK HlsSinkType = iota
	HLSSINK2
)

func (s HlsSinkType) IsValid() bool {
	switch s {
	case HLSSINK, HLSSINK2:
		return true
	}
	return false
}

const (
	RTMPSINK RtmpSinkType = iota
	RTMP2SINK
)

func (s RtmpSinkType) IsValid() bool {
	switch s {
	case RTMPSINK, RTMP2SINK:
		return true
	}
	return false
}

const (
	RTMPSRC RtmpSrcType = iota
	RTMP2SRC
)

func (s RtmpSrcType) IsValid() bool {
	switch s {
	case RTMPSRC, RTMP2SRC:
		return true
	}
	return false
}

const (
	QP_AUDIO QualityPrefer = iota
	QP_VIDEO
	QP_BOTH
)

func (s QualityPrefer) IsValid() bool {
	switch s {
	case QP_AUDIO, QP_VIDEO, QP_BOTH:
		return true
	}
	return false
}

const (
	NONE SrtMode = iota
	CALLER
	LISTENER
	RENDEZVOUS
)

func (s SrtMode) IsValid() bool {
	switch s {
	case NONE, CALLER, LISTENER, RENDEZVOUS:
		return true
	}
	return false
}

const (
	GSTREAMER UserAgent = iota
	VLC
	FFMPEG
	WINK
	CHROME
	MOZILLA
	SAFARI
)

func (s UserAgent) IsValid() bool {
	switch s {
	case GSTREAMER, VLC, FFMPEG, WINK, CHROME, MOZILLA, SAFARI:
		return true
	}
	return false
}
