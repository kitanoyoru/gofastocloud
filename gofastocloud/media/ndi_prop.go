package media

import (
	"encoding/json"
	"errors"
)

type NDIProp struct {
	Name string `json:"name"`
}

func NewNDIProp(name string) *NDIProp {
	return &NDIProp{
		Name: name,
	}
}

func (p *NDIProp) Equals(other *NDIProp) bool {
	return p.Name == other.Name
}

func (p *NDIProp) UnmarshalJSON(data []byte) error {
	req := struct {
		NdiName *string `json:"name"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.NdiName == nil {
		return errors.New("name field required")
	}

	p.Name = *req.NdiName
	return nil
}
