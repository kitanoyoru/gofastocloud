package media

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type PingResponse struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

func NewPingResponse() *PingResponse {
	return &PingResponse{Timestamp: gofastogt.MakeUTCTimestamp()}
}

func (ping *PingResponse) toBytes() (json.RawMessage, error) {
	return json.Marshal(ping)
}

func (ping *PingResponse) toResponse(sid *gofastocloud_base.RPCId) (*gofastocloud_base.Response, error) {
	data, err := ping.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewResponseMessage(sid, &data)
	return rpc, nil
}
