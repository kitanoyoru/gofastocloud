package media

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Activate

type ActivateRequest struct {
	LicenseKey gofastocloud_base.LicenseKey `json:"license_key"`
}

func (activate *ActivateRequest) UnmarshalJSON(data []byte) error {
	required := struct {
		LicenseKey *gofastocloud_base.LicenseKey `json:"license_key"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.LicenseKey == nil {
		return errors.New("license_key field required")
	}

	if !required.LicenseKey.IsValid() {
		return errors.New("invlaid license_key")
	}

	activate.LicenseKey = *required.LicenseKey
	return nil
}

func NewActivateRequest(licenseKey gofastocloud_base.LicenseKey) *ActivateRequest { // +
	return &ActivateRequest{LicenseKey: licenseKey}
}

func NewActivateRequestFromBytes(data []byte) (*ActivateRequest, error) {
	var result ActivateRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (activate *ActivateRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(activate)
}

func (activate *ActivateRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := activate.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kActivateKeyCommand, &data)
	return rpc, nil
}

// Ping

type PingRequest struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

func NewPingRequest() *PingRequest { // +
	return &PingRequest{Timestamp: gofastogt.MakeUTCTimestamp()}
}

func NewPingRequestFromBytes(data []byte) (*PingRequest, error) {
	var result PingRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ping *PingRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(ping)
}

func (ping *PingRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := ping.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kServicePingCommand, &data)
	return rpc, nil
}

// Stop service

type StopServiceRequest struct {
	Delay uint64 `json:"delay"`
}

func NewStopServiceRequest(delay uint64) *StopServiceRequest {
	return &StopServiceRequest{delay}
}

func (stop *StopServiceRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(stop)
}

func (stop *StopServiceRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := stop.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kStopServiceCommand, &data)
	return rpc, nil
}

// GetLog service

type GetLogServiceRequest struct {
	Path string `json:"path"`
}

func NewGetLogServiceRequest(path string) *GetLogServiceRequest {
	return &GetLogServiceRequest{path}
}

func (log *GetLogServiceRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(log)
}

func (log *GetLogServiceRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := log.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kGetLogServiceCommand, &data)
	return rpc, nil
}

// Inject master stream

type InjectMasterInputUrlRequest struct {
	Sid StreamId `json:"id"`
	Url InputUri `json:"url"`
}

func NewInjectMasterInputUrlRequest(sid StreamId, url InputUri) *InjectMasterInputUrlRequest { // +
	return &InjectMasterInputUrlRequest{sid, url}
}

func NewInjectMasterInputUrlRequestFromBytes(data []byte) (*InjectMasterInputUrlRequest, error) {
	var result InjectMasterInputUrlRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (inject *InjectMasterInputUrlRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(inject)
}

func (inject *InjectMasterInputUrlRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := inject.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kInjectMasterInputUrl, &data)
	return rpc, nil
}

// Remove master stream

type RemoveMasterInputUrlRequest struct {
	Sid StreamId `json:"id"`
	Url InputUri `json:"url"`
}

func NewRemoveMasterInputUrlRequest(sid StreamId, url InputUri) *RemoveMasterInputUrlRequest { // +
	return &RemoveMasterInputUrlRequest{sid, url}
}

func NewRemoveMasterInputUrlRequestFromBytes(data []byte) (*RemoveMasterInputUrlRequest, error) {
	var result RemoveMasterInputUrlRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (remove *RemoveMasterInputUrlRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(remove)
}

func (remove *RemoveMasterInputUrlRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := remove.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kRemoveMasterInputUrl, &data)
	return rpc, nil
}

// Get hardware hash

type HardwareHashRequest struct {
	Algo gofastogt.AlgoType `json:"algo"`
}

func NewHardwareHashRequest(algo gofastogt.AlgoType) *HardwareHashRequest { // +
	return &HardwareHashRequest{algo}
}

func NewHardwareHashRequestFromBytes(data []byte) (*HardwareHashRequest, error) {
	var result HardwareHashRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (probe *HardwareHashRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(probe)
}

func (probe *HardwareHashRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := probe.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kGetHardwareHashCommand, &data)
	return rpc, nil
}

// Probe in stream

type ProbeInStreamRequest struct {
	Url InputUri `json:"url"`
}

func NewProbeInStreamRequest(url InputUri) *ProbeInStreamRequest { // +
	return &ProbeInStreamRequest{url}
}

func NewProbeInStreamRequestFromBytes(data []byte) (*ProbeInStreamRequest, error) {
	var result ProbeInStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (probe *ProbeInStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(probe)
}

func (probe *ProbeInStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := probe.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kProbeInStreamCommand, &data)
	return rpc, nil
}

// Probe out stream

type ProbeOutStreamRequest struct {
	Url OutputUri `json:"url"`
}

func NewProbeOutStreamRequest(url OutputUri) *ProbeOutStreamRequest { // +
	return &ProbeOutStreamRequest{url}
}

func NewProbeOutStreamRequestFromBytes(data []byte) (*ProbeOutStreamRequest, error) {
	var result ProbeOutStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (probe *ProbeOutStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(probe)
}

func (probe *ProbeOutStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := probe.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kProbeOutStreamCommand, &data)
	return rpc, nil
}

// Mount s3

type MountS3BucketRequest struct {
	Name   string `json:"name"`
	Path   string `json:"path"`
	Key    string `json:"key"`
	Secret string `json:"secret"`
}

func NewMountS3BucketRequest(name string, path string, key string, secret string) *MountS3BucketRequest { // +
	return &MountS3BucketRequest{name, path, key, secret}
}

func NewMountS3BucketRequestFromBytes(data []byte) (*MountS3BucketRequest, error) {
	var result MountS3BucketRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (mount *MountS3BucketRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(mount)
}

func (mount *MountS3BucketRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := mount.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kMountS3BucketCommand, &data)
	return rpc, nil
}

// Unmount s3

type UnMountS3BucketRequest struct {
	Path string `json:"path"`
}

func NewUnMountS3BucketRequest(path string) *UnMountS3BucketRequest { // +
	return &UnMountS3BucketRequest{Path: path}
}

func NewUnMountS3BucketRequestFromBytes(data []byte) (*UnMountS3BucketRequest, error) {
	var result UnMountS3BucketRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (mount *UnMountS3BucketRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(mount)
}

func (mount *UnMountS3BucketRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := mount.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kUnMountS3BucketCommand, &data)
	return rpc, nil
}

type ScanS3BucketsRequest struct {
}

func NewScanS3BucketsRequest() *ScanS3BucketsRequest { // +
	return &ScanS3BucketsRequest{}
}

func NewScanS3BucketsRequestFromBytes(data []byte) (*ScanS3BucketsRequest, error) {
	var result ScanS3BucketsRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (scan *ScanS3BucketsRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(scan)
}

func (scan *ScanS3BucketsRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := scan.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kScanS3BucketsCommand, &data)
	return rpc, nil
}

// Scan folder

type ScanFolderRequest struct {
	Directory  string   `json:"directory"`
	Extensions []string `json:"extensions"`
}

func NewScanFolderRequest(directory string, extensions []string) *ScanFolderRequest { // +
	return &ScanFolderRequest{Directory: directory, Extensions: extensions}
}

func NewScanFolderRequestFromBytes(data []byte) (*ScanFolderRequest, error) {
	var result ScanFolderRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (scan *ScanFolderRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(scan)
}

func (scan *ScanFolderRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := scan.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kScanFolderCommand, &data)
	return rpc, nil
}

func (scan *ScanFolderRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Directory  string   `json:"directory"`
		Extensions []string `json:"extensions"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if len(request.Directory) == 0 {
		return errors.New("directory can't be empty")
	}
	if len(request.Extensions) == 0 {
		return errors.New("extensions can't be empty")
	}
	scan.Directory = request.Directory
	scan.Extensions = request.Extensions
	return nil
}

// Start stream

type StartStreamRequest struct {
	Config json.RawMessage `json:"config"`
}

func NewStartProxyStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodProxyStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartRestreamStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartEncodeStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartTimeshiftPlayerRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartTimeshiftRecorderRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCatchupRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartTestLifeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodRelayRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCodRelayRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCodEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartEventRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCvDataRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartChangerRelayequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartChangerEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartLiteStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func (start *StartStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(start)
}

func (start *StartStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := start.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kStartStreamCommand, &data)
	return rpc, nil
}

// Stop stream

type StopStreamRequest struct {
	Sid   StreamId `json:"id"`
	Force bool     `json:"force"`
}

func (stop *StopStreamRequest) UnmarshalJSON(data []byte) error {
	required := struct {
		Sid   *StreamId `json:"id"`
		Force *bool     `json:"force"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Sid == nil {
		return errors.New("id field required")
	}

	if len(*required.Sid) == 0 {
		return errors.New("id can't be empty")
	}

	if required.Force == nil {
		return errors.New("force field required")
	}

	stop.Sid = *required.Sid
	stop.Force = *required.Force
	return nil
}

func NewStopStreamRequest(sid StreamId, force bool) *StopStreamRequest {
	return &StopStreamRequest{sid, force}
}

func NewStopStreamRequestFromBytes(data []byte) (*StopStreamRequest, error) {
	var result StopStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (stop *StopStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(stop)
}

func (stop *StopStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := stop.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kStopStreamCommand, &data)
	return rpc, nil
}

// Restart stream

type RestartStreamRequest struct {
	Sid StreamId `json:"id"`
}

func (restart *RestartStreamRequest) UnmarshalJSON(data []byte) error {
	required := struct {
		Sid *StreamId `json:"id"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Sid == nil {
		return errors.New("id field required")
	}

	if len(*required.Sid) == 0 {
		return errors.New("id can't be empty")
	}

	restart.Sid = *required.Sid
	return nil
}

func NewRestartStreamRequest(sid StreamId) *RestartStreamRequest {
	return &RestartStreamRequest{sid}
}

func NewRestartStreamRequestFromBytes(data []byte) (*RestartStreamRequest, error) {
	var result RestartStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (restart *RestartStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(restart)
}

func (restart *RestartStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := restart.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kRestartStreamCommand, &data)
	return rpc, nil
}

// Clean stream

type CleanStreamRequest struct {
	Config json.RawMessage `json:"config"`
}

func NewCleanStreamRequest(config json.RawMessage) *CleanStreamRequest { // +
	return &CleanStreamRequest{Config: config}
}

func NewCleanStreamRequestFromBytes(data []byte) (*CleanStreamRequest, error) {
	var clean CleanStreamRequest
	if err := json.Unmarshal(data, &clean); err != nil {
		return nil, err
	}
	return &clean, nil
}

func (clean *CleanStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(clean)
}

func (clean *CleanStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := clean.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kCleanStreamCommand, &data)
	return rpc, nil
}

// GetLog stream

type GetLogStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
	Path        string   `json:"path"`
}

func NewGetLogStreamRequest(sid StreamId, feedbackDir string, path string) *GetLogStreamRequest {
	return &GetLogStreamRequest{sid, feedbackDir, path}
}

func (log *GetLogStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(log)
}

func (log *GetLogStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := log.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kGetLogStreamCommand, &data)
	return rpc, nil
}

func (log *GetLogStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
		Path        *string   `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}

	log.Sid = *request.Sid
	log.FeedbackDir = *request.FeedbackDir
	log.Path = *request.Path
	return nil
}

// GetPipeline stream

type GetPipelineStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
	Path        string   `json:"path"`
}

func NewGetPipelineStreamRequest(sid StreamId, feedbackDir string, path string) *GetPipelineStreamRequest {
	return &GetPipelineStreamRequest{sid, feedbackDir, path}
}

func (pipe *GetPipelineStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(pipe)
}

func (pipe *GetPipelineStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := pipe.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kGetPipelineStreamCommand, &data)
	return rpc, nil
}

func (pipe *GetPipelineStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
		Path        *string   `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}

	pipe.Sid = *request.Sid
	pipe.FeedbackDir = *request.FeedbackDir
	pipe.Path = *request.Path
	return nil
}

// GetConfigJson stream

type GetConfigJsonStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
	Path        string   `json:"path"`
}

func NewGetConfigJsonStreamRequest(sid StreamId, feedbackDir string, path string) *GetConfigJsonStreamRequest {
	return &GetConfigJsonStreamRequest{sid, feedbackDir, path}
}

func (config *GetConfigJsonStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(config)
}

func (config *GetConfigJsonStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := config.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kGetConfigJsonStreamCommand, &data)
	return rpc, nil
}

func (config *GetConfigJsonStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
		Path        *string   `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}

	config.Sid = *request.Sid
	config.FeedbackDir = *request.FeedbackDir
	config.Path = *request.Path
	return nil
}

// ChangeInputStreamRequest stream

type ChangeInputStreamRequest struct {
	Sid       StreamId `json:"id"`
	ChannelId int      `json:"channel_id"`
}

func NewChangeInputStreamRequest(sid StreamId, channelId int) *ChangeInputStreamRequest {
	return &ChangeInputStreamRequest{sid, channelId}
}

func NewChangeInputStreamRequestFromBytes(data []byte) (*ChangeInputStreamRequest, error) {
	var result ChangeInputStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (change *ChangeInputStreamRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(change)
}

func (change *ChangeInputStreamRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := change.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kChangeInputStreamCommand, &data)
	return rpc, nil
}

func (change *ChangeInputStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid       *StreamId `json:"id"`
		ChannelId *int      `json:"channel_id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.ChannelId == nil {
		return errors.New("channel_id field required")
	}

	change.Sid = *request.Sid
	change.ChannelId = *request.ChannelId
	return nil
}
