package media

import "errors"

var ErrNotActive = errors.New("not active")
var ErrNotPro = errors.New("version isn't PRO")
