package media

import (
	"encoding/json"
	"net/url"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type IFastoCloudNodeClientHandler interface {
	OnClientStateChanged(client *FastoCloudNodeClient, status gofastocloud_base.ConnectionStatus)
	OnStreamStatisticReceived(client *FastoCloudNodeClient, statistic StreamStatisticInfo)
	OnStreamSourcesChanged(client *FastoCloudNodeClient, source ChangedSourcesInfo)
	OnStreamMlNotification(client *FastoCloudNodeClient, notify MlNotificationInfo)
	OnStreamResultReady(client *FastoCloudNodeClient, result ResultStreamInfo)
	OnWebRTCOutInitReceived(client *FastoCloudNodeClient, web WebRTCOutInitInfo)
	OnWebRTCOutDeInitReceived(client *FastoCloudNodeClient, web WebRTCOutDeInitInfo)
	OnWebRTCOutSdpReceived(client *FastoCloudNodeClient, web WebRTCOutSdpInfo)
	OnWebRTCOutIceReceived(client *FastoCloudNodeClient, web WebRTCOutIceInfo)
	OnWebRTCInInitReceived(client *FastoCloudNodeClient, web WebRTCInInitInfo)
	OnWebRTCInDeInitReceived(client *FastoCloudNodeClient, web WebRTCInDeInitInfo)
	OnWebRTCInSdpReceived(client *FastoCloudNodeClient, web WebRTCInSdpInfo)
	OnWebRTCInIceReceived(client *FastoCloudNodeClient, web WebRTCInIceInfo)
	OnQuitStatusStream(client *FastoCloudNodeClient, status QuitStatusInfo)
	OnPingReceived(client *FastoCloudNodeClient, ping PingInfo)

	OnServiceStatisticReceived(client *FastoCloudNodeClient, statistic ServiceStatisticInfo)
	OnCDNStatisticReceived(client *FastoCloudNodeClient, statistic CDNStatisticInfo)
}

type FastoCloudNodeStandardObserver struct {
	gofastocloud_base.IClientObserver

	parent *FastoCloudNodeClient
}

type FastoCloudNodeClient struct {
	client    *FastoCloudClient
	handler   IFastoCloudNodeClientHandler
	requestId gofastogt.UniqueIDU64
}

func NewFastoCloudNodeClient(host gofastogt.HostAndPort, handler IFastoCloudNodeClientHandler) *FastoCloudNodeClient {
	fasto := FastoCloudNodeStandardObserver{}
	client := NewFastoCloudClient(host, &fasto)
	p := FastoCloudNodeClient{client, handler, gofastogt.UniqueIDU64{}}
	fasto.parent = &p
	return &p
}

func (client *FastoCloudNodeClient) GetHttpHost() *url.URL {
	return client.client.GetHttpHost()
}

func (client *FastoCloudNodeClient) GetCodsHost() *url.URL {
	return client.client.GetCodsHost()
}

func (client *FastoCloudNodeClient) GetRuntimeStats() *ServiceStatisticInfo {
	return client.client.GetRuntimeStats()
}

func (client *FastoCloudNodeClient) GetVodsHost() *url.URL {
	return client.client.GetVodsHost()
}

func (client *FastoCloudNodeClient) GetProject() *string {
	return client.client.GetProject()
}

func (client *FastoCloudNodeClient) GetVersion() *string {
	return client.client.GetVersion()
}

func (client *FastoCloudNodeClient) GetExpirationTime() *gofastogt.UtcTimeMsec {
	return client.client.GetExpirationTime()
}

func (client *FastoCloudNodeClient) GetOS() *gofastocloud_base.OperationSystem {
	return client.client.GetOS()
}

func (client *FastoCloudNodeClient) IsActive() bool {
	return client.client.IsActive()
}

func (client *FastoCloudNodeClient) GetStatus() gofastocloud_base.ConnectionStatus {
	return client.client.GetStatus()
}

func (client *FastoCloudNodeClient) IsConnected() bool {
	return client.client.IsConnected()
}

func (client *FastoCloudNodeClient) Connect() error {
	return client.client.Connect()
}

func (client *FastoCloudNodeClient) Flush() {
	client.client.Flush()
}

func (client *FastoCloudNodeClient) Disconnect() error {
	return client.client.Disconnect()
}

func (client *FastoCloudNodeClient) generateRequestId() uint64 {
	return client.requestId.Get()
}

func (client *FastoCloudNodeClient) Activate(activate *ActivateRequest) (*gofastocloud_base.RPCId, error) {
	return client.ActivateWithCallback(activate, nil)
}

func (client *FastoCloudNodeClient) ActivateWithCallback(activate *ActivateRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ActivateWithCallback(client.generateRequestId(), activate, callback)
}

func (client *FastoCloudNodeClient) WebRTCOutInitStream(web *WebRTCOutInitRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCOutInitStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCOutInitStreamWithCallback(web *WebRTCOutInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCOutInitStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCOutDeinitStream(web *WebRTCOutDeInitRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCOutDeinitStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCOutDeinitStreamWithCallback(web *WebRTCOutDeInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCOutDeinitStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCOutSDPStream(web *WebRTCOutSdpRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCOutSDPStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCOutSDPStreamWithCallback(web *WebRTCOutSdpRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCOutSDPStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCOutICEStream(web *WebRTCOutIceRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCOutICEStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCOutICEStreamWithCallback(web *WebRTCOutIceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCOutICEStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCInInitStream(web *WebRTCInInitRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCInInitStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCInInitStreamWithCallback(web *WebRTCInInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCInInitStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCInDeinitStream(web *WebRTCInDeInitRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCInDeinitStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCInDeinitStreamWithCallback(web *WebRTCInDeInitRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCInDeinitStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCInSDPStream(web *WebRTCInSdpRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCInSDPStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCInSDPStreamWithCallback(web *WebRTCInSdpRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCInSDPStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) WebRTCInICEStream(web *WebRTCInIceRequest) (*gofastocloud_base.RPCId, error) {
	return client.WebRTCInICEStreamWithCallback(web, nil)
}

func (client *FastoCloudNodeClient) WebRTCInICEStreamWithCallback(web *WebRTCInIceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.WebRTCInICEStreamWithCallback(client.generateRequestId(), web, callback)
}

func (client *FastoCloudNodeClient) InjectMasterInputUrl(inject *InjectMasterInputUrlRequest) (*gofastocloud_base.RPCId, error) {
	return client.InjectMasterInputUrlWithCallback(inject, nil)
}

func (client *FastoCloudNodeClient) InjectMasterInputUrlWithCallback(inject *InjectMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.InjectMasterInputUrlWithCallback(client.generateRequestId(), inject, callback)
}

func (client *FastoCloudNodeClient) RemoveMasterInputUrl(remove *RemoveMasterInputUrlRequest) (*gofastocloud_base.RPCId, error) {
	return client.RemoveMasterInputUrlWithCallback(remove, nil)
}

func (client *FastoCloudNodeClient) RemoveMasterInputUrlWithCallback(remove *RemoveMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.RemoveMasterInputUrlWithCallback(client.generateRequestId(), remove, callback)
}

func (client *FastoCloudNodeClient) ScanS3Buckets(scan *ScanS3BucketsRequest) (*gofastocloud_base.RPCId, error) {
	return client.ScanS3BucketsWithCallback(scan, nil)
}

func (client *FastoCloudNodeClient) ScanS3BucketsWithCallback(scan *ScanS3BucketsRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ScanS3BucketsWithCallback(client.generateRequestId(), scan, callback)
}

func (client *FastoCloudNodeClient) ScanFolder(scan *ScanFolderRequest) (*gofastocloud_base.RPCId, error) {
	return client.ScanFolderWithCallback(scan, nil)
}

func (client *FastoCloudNodeClient) ScanFolderWithCallback(scan *ScanFolderRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ScanFolderWithCallback(client.generateRequestId(), scan, callback)
}

func (client *FastoCloudNodeClient) CleanStream(clean *CleanStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.CleanStreamWithCallback(clean, nil)
}

func (client *FastoCloudNodeClient) CleanStreamWithCallback(clean *CleanStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.CleanStreamWithCallback(client.generateRequestId(), clean, callback)
}

func (client *FastoCloudNodeClient) Ping() (*gofastocloud_base.RPCId, error) {
	return client.PingWithCallback(nil)
}

func (client *FastoCloudNodeClient) PingWithCallback(callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.PingWithCallback(client.generateRequestId(), NewPingRequest(), callback)
}

func (client *FastoCloudNodeClient) StopService(request *StopServiceRequest) (*gofastocloud_base.RPCId, error) {
	return client.StopServiceWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) StopServiceWithCallback(request *StopServiceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.StopServiceWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) GetLogService(request *GetLogServiceRequest) (*gofastocloud_base.RPCId, error) {
	return client.GetLogServiceWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) GetLogServiceWithCallback(request *GetLogServiceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.GetLogServiceWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) ProbeInStream(request *ProbeInStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.ProbeInStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) ProbeInStreamWithCallback(request *ProbeInStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ProbeInStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) GetHardwareHash(request *HardwareHashRequest) (*gofastocloud_base.RPCId, error) {
	return client.GetHardwareHashWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) GetHardwareHashWithCallback(request *HardwareHashRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.GetHardwareHashRequestWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) ProbeOutStream(request *ProbeOutStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.ProbeOutStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) ProbeOutStreamWithCallback(request *ProbeOutStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ProbeOutStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) MountS3Bucket(request *MountS3BucketRequest) (*gofastocloud_base.RPCId, error) {
	return client.MountS3BucketWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) MountS3BucketWithCallback(request *MountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.MountS3BucketWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) UnMountS3Bucket(request *UnMountS3BucketRequest) (*gofastocloud_base.RPCId, error) {
	return client.UnMountS3BucketWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) UnMountS3BucketWithCallback(request *UnMountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.UnMountS3BucketWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) StartStream(request *StartStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.StartStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) StartStreamWithCallback(request *StartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.StartStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) StopStream(request *StopStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.StopStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) StopStreamWithCallback(request *StopStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.StopStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) RestartStream(request *RestartStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.RestartStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) RestartStreamWithCallback(request *RestartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.RestartStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) ChangeInputStream(request *ChangeInputStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.ChangeInputStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) ChangeInputStreamWithCallback(request *ChangeInputStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.ChangeInputStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) GetLogStream(request *GetLogStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.GetLogStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) GetLogStreamWithCallback(request *GetLogStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.GetLogStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) GetPipelineStream(request *GetPipelineStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.GetPipelineStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) GetPipelineStreamWithCallback(request *GetPipelineStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.GetPipelineStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) GetConfigJsonStream(request *GetConfigJsonStreamRequest) (*gofastocloud_base.RPCId, error) {
	return client.GetConfigJsonStreamWithCallback(request, nil)
}

func (client *FastoCloudNodeClient) GetConfigJsonStreamWithCallback(request *GetConfigJsonStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	return client.client.GetConfigJsonStreamWithCallback(client.generateRequestId(), request, callback)
}

func (client *FastoCloudNodeClient) ReadCommand() ([]byte, error) {
	return client.client.ReadCommand()
}

func (client *FastoCloudNodeClient) ProcessCommand(data []byte) {
	client.client.ProcessCommand(data)
}

func (observer *FastoCloudNodeStandardObserver) OnClientStateChanged(status gofastocloud_base.ConnectionStatus) {
	if observer.parent != nil {
		if status != gofastocloud_base.ACTIVE {
			observer.parent.client.Reset()
		}

		if observer.parent.handler != nil {
			observer.parent.handler.OnClientStateChanged(observer.parent, status)
		}
	}
}

func (observer *FastoCloudNodeStandardObserver) ProcessRequest(request *gofastocloud_base.Request) {
	if request == nil {
		return
	}

	if request.Method == kStatisticStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var statistic StreamStatisticInfo
			if err := json.Unmarshal(*request.Params, &statistic); err == nil {
				observer.parent.handler.OnStreamStatisticReceived(observer.parent, statistic)
			}
		}
	} else if request.Method == kStatisticServiceBroadcastCommand {
		if observer.parent.handler != nil {
			var statistic ServiceStatisticInfo
			if err := json.Unmarshal(*request.Params, &statistic); err == nil {
				observer.parent.handler.OnServiceStatisticReceived(observer.parent, statistic)
			}
		}
	} else if request.Method == kStatisticCDNBroadcastCommand {
		if observer.parent.handler != nil {
			var statistic CDNStatisticInfo
			if err := json.Unmarshal(*request.Params, &statistic); err == nil {
				observer.parent.handler.OnCDNStatisticReceived(observer.parent, statistic)
			}
		}
	} else if request.Method == kChangedStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var source ChangedSourcesInfo
			if err := json.Unmarshal(*request.Params, &source); err == nil {
				observer.parent.handler.OnStreamSourcesChanged(observer.parent, source)
			}
		}
	} else if request.Method == kMlNotificationStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var res MlNotificationInfo
			if err := json.Unmarshal(*request.Params, &res); err == nil {
				observer.parent.handler.OnStreamMlNotification(observer.parent, res)
			}
		}
	} else if request.Method == kResultStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var res ResultStreamInfo
			if err := json.Unmarshal(*request.Params, &res); err == nil {
				observer.parent.handler.OnStreamResultReady(observer.parent, res)
			}
		}
	} else if request.Method == kWebRTCOutInitStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCOutInitInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCOutInitReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCOutDeInitStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCOutDeInitInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCOutDeInitReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCOutSdpStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCOutSdpInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCOutSdpReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCOutIceStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCOutIceInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCOutIceReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCInInitStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCInInitInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCInInitReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCInDeInitStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCInDeInitInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCInDeInitReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCInSdpStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCInSdpInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCInSdpReceived(observer.parent, web)
			}
		}
	} else if request.Method == kWebRTCInIceStreamBroadcastCommand {
		if observer.parent.handler != nil {
			var web WebRTCInIceInfo
			if err := json.Unmarshal(*request.Params, &web); err == nil {
				observer.parent.handler.OnWebRTCInIceReceived(observer.parent, web)
			}
		}
	} else if request.Method == kQuitStatusStreamBroadcastCommand {
		var quit QuitStatusInfo
		if err := json.Unmarshal(*request.Params, &quit); err == nil {
			observer.parent.handler.OnQuitStatusStream(observer.parent, quit)
		}
	} else if request.Method == kClientPingCommand {
		if observer.parent.handler != nil {
			var ping PingInfo
			if err := json.Unmarshal(*request.Params, &ping); err == nil {
				observer.parent.handler.OnPingReceived(observer.parent, ping)
			}
		}
	}
}

func (observer *FastoCloudNodeStandardObserver) ProcessResponse(request *gofastocloud_base.Request, response *gofastocloud_base.Response) {
	if request == nil {
		return
	}

	if request.Method == kActivateKeyCommand {
		if response.IsMessage() {
			if observer.parent.handler != nil {
				var statistic ServiceStatisticInfo
				if err := json.Unmarshal(*response.Result, &statistic); err == nil {
					observer.parent.handler.OnServiceStatisticReceived(observer.parent, statistic)
				}
			}
		}
	}
}
