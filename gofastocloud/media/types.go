package media

import "gitlab.com/fastogt/gofastogt/gofastogt"

type Bitrate int
type Volume float64

type StreamLink struct {
	HttpProxy  *string       `bson:"http_proxy,omitempty" json:"http_proxy,omitempty"`
	HttpsProxy *string       `bson:"https_proxy,omitempty" json:"https_proxy,omitempty"`
	Prefer     QualityPrefer `bson:"prefer" json:"prefer"`
}

type SrtKey struct {
	Passphrase string `bson:"passphrase" json:"passphrase"`
	KeyLen     int    `bson:"pbkeylen" json:"pbkeylen"`
}

type DrmKey struct {
	Kid string `bson:"kid" json:"kid"`
	Key string `bson:"key" json:"key"`
}

type WebRTCProp struct {
	Stun string `bson:"stun" json:"stun"`
	Turn string `bson:"turn" json:"turn"`
}

type KvsProp struct {
	StreamName  string `bson:"stream_name" json:"stream_name"`
	AccessKey   string `bson:"access_key" json:"access_key"`
	SecretKey   string `bson:"secret_key" json:"secret_key"`
	AwsRegion   string `bson:"aws_region" json:"aws_region"`
	StorageSize int    `bson:"storage_size" json:"storage_size"`
}

type AzureProp struct {
	AccountName string `bson:"account_name" json:"account_name"`
	AccountKey  string `bson:"account_key" json:"account_key"`
	Location    string `bson:"location" json:"location"`
}

type GoogleProp struct {
	AccountCreds string `bson:"account_creds" json:"account_creds"`
}

type MlModel struct {
	Path string `json:"path"`
}

type MachineLearning struct {
	Backend  int       `json:"backend"`
	Models   []MlModel `json:"models"`
	Tracking bool      `json:"tracking"`
	Overlay  bool      `json:"overlay"`
}

type Logo struct {
	Path     string          `json:"path"`
	Position gofastogt.Point `json:"position"`
	Alpha    float64         `json:"alpha"`
	Size     gofastogt.Size  `json:"size"`
}

type RSVGLogo struct {
	Path     string          `json:"path"`
	Position gofastogt.Point `json:"position"`
	Size     gofastogt.Size  `json:"size"`
}

type BackgroundEffect struct {
	Type     BackgroundEffectType `json:"type"`
	Strength *float64             `json:"strength,omitempty"`
	Image    *string              `json:"image,omitempty"`
	Color    *int                 `json:"color,omitempty"`
}

type AudioStabilization struct {
	Model GPUModel        `bson:"gpu_model" json:"gpu_model"`
	Type  AudioEffectType `bson:"type" json:"type"`
}

type Font struct {
	Family string `json:"family"`
	Size   int    `json:"size"`
}

type TextOverlay struct {
	Text      string  `json:"text"`
	XAbsolute float64 `json:"x_absolute"`
	YAbsolute float64 `json:"y_absolute"`
	Font      *Font   `json:"font,omitempty"`
}

type ExtraConfig map[string]interface{}

type MediaInfo struct {
	Rotation *int                   `json:"rotation,omitempty"`
	Width    int                    `json:"width"`
	Height   int                    `json:"height"`
	Duration gofastogt.DurationMsec `json:"duration"` // msec
}

func (info *MediaInfo) VideoFlip() VideoFlip {
	if info.Rotation == nil {
		return 0
	}

	rotation := *info.Rotation
	return VideoFlipFromRotation(rotation)
}

// Media file info

type MediaUrlInfo struct {
	MediaInfo
	Url string `json:"url"`
}

func NewMediaUrlInfo(info MediaInfo, url string) *MediaUrlInfo {
	minfo := MediaUrlInfo{info, url}
	return &minfo
}

type Programme struct {
	Channel     string                `bson:"channel" json:"channel"`
	Start       gofastogt.UtcTimeMsec `bson:"start" json:"start"`
	Stop        gofastogt.UtcTimeMsec `bson:"stop" json:"stop"`
	Title       string                `bson:"title" json:"title"`
	Category    *string               `bson:"category,omitempty" json:"category,omitempty"`
	Description *string               `bson:"desc,omitempty" json:"desc,omitempty"`
	Icon        *string               `bson:"icon,omitempty" json:"icon,omitempty"`
}

type ProgramInfo struct {
	Programme
}
