package media

type VideoParser string

const (
	TS_PARSE   VideoParser = "tsparse"
	H264_PARSE VideoParser = "h264parse"
	H265_PARSE VideoParser = "h265parse"
	VP8_PARSE  VideoParser = "vp8parse"
	VP9_PARSE  VideoParser = "vp9parse"
)

func (parser VideoParser) IsValid() bool {
	switch parser {
	case TS_PARSE, H264_PARSE, H265_PARSE, VP8_PARSE, VP9_PARSE:
		return true
	}
	return false
}

type AudioParser string

const (
	MPEG_PARSER     AudioParser = "mpegaudioparse"
	AAC_PARSE       AudioParser = "aacparse"
	AC3_PARSE       AudioParser = "ac3parse"
	OPUS_PARSE      AudioParser = "opusparse"
	RAW_AUDIO_PARSE AudioParser = "rawaudioparse"
)

func (parser AudioParser) IsValid() bool {
	switch parser {
	case MPEG_PARSER, AAC_PARSE, AC3_PARSE, OPUS_PARSE, RAW_AUDIO_PARSE:
		return true
	}
	return false
}

type VideoCodec string

const (
	EAVC_ENC         VideoCodec = "eavcenc"
	OPEN_H264_ENC    VideoCodec = "openh264enc"
	X264_ENC         VideoCodec = "x264enc"
	NV_H264_ENC      VideoCodec = "nvh264enc"
	NV_H265_ENC      VideoCodec = "nvh265enc"
	VAAPI_H264_ENC   VideoCodec = "vaapih264enc"
	VAAPI_H265_ENC   VideoCodec = "vaapih265enc"
	VAAPI_MPEG2_ENC  VideoCodec = "vaapimpeg2enc"
	VAAPI_VP8_ENC    VideoCodec = "vaapivp8enc"
	VAAPI_VP9_ENC    VideoCodec = "vaapivp9enc"
	MFX_H264_ENC     VideoCodec = "mfxh264enc"
	X265_ENC         VideoCodec = "x265enc"
	MSDK_H264_ENC    VideoCodec = "msdkh264enc"
	VP8_ENC          VideoCodec = "vp8enc"
	VP9_ENC          VideoCodec = "vp9enc"
	NVV_4L2_H264_ENC VideoCodec = "nvv4l2h264enc"
	NVV_4L2_H265_ENC VideoCodec = "nvv4l2h265enc"
	NVV_4L2_VP8_ENC  VideoCodec = "nvv4l2vp8enc"
	NVV_4L2_VP9_ENC  VideoCodec = "nvv4l2vp9enc"
)

func (codec VideoCodec) IsValid() bool {
	switch codec {
	case EAVC_ENC, OPEN_H264_ENC, X264_ENC, NV_H264_ENC, NV_H265_ENC, VAAPI_H264_ENC, VAAPI_H265_ENC, VAAPI_MPEG2_ENC, VAAPI_VP8_ENC, VAAPI_VP9_ENC, MFX_H264_ENC, X265_ENC, MSDK_H264_ENC, VP8_ENC, VP9_ENC,
		NVV_4L2_H264_ENC, NVV_4L2_H265_ENC, NVV_4L2_VP8_ENC, NVV_4L2_VP9_ENC:
		return true
	}
	return false
}

type AudioCodec string

const (
	LAME_MP3_ENC AudioCodec = "lamemp3enc"
	FAAC         AudioCodec = "faac"
	VOAAC_ENC    AudioCodec = "voaacenc"
	OPUS_ENC     AudioCodec = "opusenc"
)

func (codec AudioCodec) IsValid() bool {
	switch codec {
	case LAME_MP3_ENC, FAAC, VOAAC_ENC, OPUS_ENC:
		return true
	}
	return false
}
