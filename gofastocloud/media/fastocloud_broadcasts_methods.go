package media

// streams
const kQuitStatusStreamBroadcastCommand = "quit_status_stream"
const kStatisticStreamBroadcastCommand = "statistic_stream"
const kChangedStreamBroadcastCommand = "changed_source_stream"
const kMlNotificationStreamBroadcastCommand = "ml_notification_stream"
const kResultStreamBroadcastCommand = "result_stream"

// webrtc
const kWebRTCOutInitStreamBroadcastCommand = "webrtc_out_init_stream"
const kWebRTCOutSdpStreamBroadcastCommand = "webrtc_out_sdp_stream"
const kWebRTCOutIceStreamBroadcastCommand = "webrtc_out_ice_stream"
const kWebRTCOutDeInitStreamBroadcastCommand = "webrtc_out_deinit_stream"
const kWebRTCInInitStreamBroadcastCommand = "webrtc_in_init_stream"
const kWebRTCInSdpStreamBroadcastCommand = "webrtc_in_sdp_stream"
const kWebRTCInIceStreamBroadcastCommand = "webrtc_in_ice_stream"
const kWebRTCInDeInitStreamBroadcastCommand = "webrtc_in_deinit_stream"

// service
const kStatisticServiceBroadcastCommand = "statistic_service"

// cdn
const kStatisticCDNBroadcastCommand = "statistic_cdn"
