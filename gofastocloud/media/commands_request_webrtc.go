package media

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
)

// WebRTC out init

type WebRTCOutInitRequest struct {
	Sid  StreamId             `json:"id"`
	Init WebRTCSubInitRequest `json:"init"`
}

type WebRTCSubInitRequest struct {
	ConnectionId string      `json:"connection_id"`
	WebRTC       *WebRTCProp `json:"webrtc,omitempty"`
}

func NewWebRTCOutInitRequest(sid StreamId, init WebRTCSubInitRequest) *WebRTCOutInitRequest {
	return &WebRTCOutInitRequest{sid, init}
}

func (web *WebRTCOutInitRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCOutInitRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCOutInitStreamCommand, &data)
	return rpc, nil
}

// WebRTC out deinit

type WebRTCOutDeInitRequest struct {
	Sid    StreamId               `json:"id"`
	DeInit WebRTCSubDeInitRequest `json:"init"`
}

type WebRTCSubDeInitRequest struct {
	ConnectionId string `json:"connection_id"`
}

func NewWebRTCOutDeInitRequest(sid StreamId, deinit WebRTCSubDeInitRequest) *WebRTCOutDeInitRequest {
	return &WebRTCOutDeInitRequest{sid, deinit}
}

func (web *WebRTCOutDeInitRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCOutDeInitRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCOutDeinitStreamCommand, &data)
	return rpc, nil
}

// WebRTC out sdp

type WebRTCOutSdpRequest struct {
	Sid         StreamId           `json:"id"`
	Description SessionDescription `json:"description"`
}

type SessionDescription struct {
	ConnectionId   string `json:"connection_id"`
	SdpDescription string `json:"sdp"`
	SdpType        string `json:"type"`
}

func NewWebRTCOutSdpRequest(sid StreamId, description SessionDescription) *WebRTCOutSdpRequest {
	return &WebRTCOutSdpRequest{sid, description}
}

func (web *WebRTCOutSdpRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCOutSdpRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCOutSdpStreamCommand, &data)
	return rpc, nil
}

// WebRTC out ice

type WebRTCOutIceRequest struct {
	Sid StreamId     `json:"id"`
	Ice IceCandidate `json:"ice"`
}

type IceCandidate struct {
	ConnectionId string `json:"connection_id"`
	Candidate    string `json:"candidate"`
	Mlindex      int    `json:"sdpMLineIndex"`
	Mid          string `json:"sdpMid"`
}

func NewWebRTCOutIceRequest(sid StreamId, ice IceCandidate) *WebRTCOutIceRequest {
	return &WebRTCOutIceRequest{sid, ice}
}

func (web *WebRTCOutIceRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCOutIceRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCOutIceStreamCommand, &data)
	return rpc, nil
}

// WebRTC in init

type WebRTCInInitRequest struct {
	Sid  StreamId             `json:"id"`
	Init WebRTCSubInitRequest `json:"init"`
}

func NewWebRTCInInitRequest(sid StreamId, init WebRTCSubInitRequest) *WebRTCInInitRequest {
	return &WebRTCInInitRequest{sid, init}
}

func (web *WebRTCInInitRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCInInitRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCInInitStreamCommand, &data)
	return rpc, nil
}

// WebRTC in deinit

type WebRTCInDeInitRequest struct {
	Sid    StreamId               `json:"id"`
	DeInit WebRTCSubDeInitRequest `json:"init"`
}

func NewWebRTCInDeInitRequest(sid StreamId, deinit WebRTCSubDeInitRequest) *WebRTCInDeInitRequest {
	return &WebRTCInDeInitRequest{sid, deinit}
}

func (web *WebRTCInDeInitRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCInDeInitRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCInDeinitStreamCommand, &data)
	return rpc, nil
}

// WebRTC in sdp

type WebRTCInSdpRequest struct {
	Sid         StreamId           `json:"id"`
	Description SessionDescription `json:"description"`
}

func NewWebRTCInSdpRequest(sid StreamId, description SessionDescription) *WebRTCInSdpRequest {
	return &WebRTCInSdpRequest{sid, description}
}

func (web *WebRTCInSdpRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCInSdpRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCInSdpStreamCommand, &data)
	return rpc, nil
}

// WebRTC in ice

type WebRTCInIceRequest struct {
	Sid StreamId     `json:"id"`
	Ice IceCandidate `json:"ice"`
}

func NewWebRTCInIceRequest(sid StreamId, ice IceCandidate) *WebRTCInIceRequest {
	return &WebRTCInIceRequest{sid, ice}
}

func (web *WebRTCInIceRequest) toBytes() (json.RawMessage, error) {
	return json.Marshal(web)
}

func (web *WebRTCInIceRequest) toRequest(sid uint64) (*gofastocloud_base.Request, error) {
	data, err := web.toBytes()
	if err != nil {
		return nil, err
	}

	rpc := gofastocloud_base.NewRequest(gofastocloud_base.NewSequenceId(sid), kWebRTCInIceStreamCommand, &data)
	return rpc, nil
}
