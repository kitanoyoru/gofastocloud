package media

import (
	"sort"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type OnlineUsers struct {
	Daemon int `json:"daemon"`
	Http   int `json:"http"`
	Vods   int `json:"vods"`
	Cods   int `json:"cods"`
}

type Machine struct {
	Cpu           float64               `bson:"cpu"             json:"cpu"`
	Gpu           float64               `bson:"gpu"             json:"gpu"`
	LoadAverage   string                `bson:"load_average"    json:"load_average"`
	MemoryTotal   uint64                `bson:"memory_total"    json:"memory_total"`
	MemoryFree    uint64                `bson:"memory_free"     json:"memory_free"`
	HddTotal      uint64                `bson:"hdd_total"       json:"hdd_total"`
	HddFree       uint64                `bson:"hdd_free"        json:"hdd_free"`
	BandwidthIn   uint64                `bson:"bandwidth_in"    json:"bandwidth_in"`
	BandwidthOut  uint64                `bson:"bandwidth_out"   json:"bandwidth_out"`
	Uptime        gofastogt.UtcTimeMsec `bson:"uptime"          json:"uptime"`
	Timestamp     gofastogt.UtcTimeMsec `bson:"timestamp"       json:"timestamp"`
	TotalBytesIn  uint64                `bson:"total_bytes_in"  json:"total_bytes_in"`
	TotalBytesOut uint64                `bson:"total_bytes_out" json:"total_bytes_out"`
}

type ServiceStatisticInfo struct {
	Machine
	OnlineUsers OnlineUsers `json:"online_users"`
}

type CDNStatisticInfo struct {
	Id               string                `json:"id"`
	RSS              int64                 `json:"rss"`
	Cpu              float64               `json:"cpu"`
	Timestamp        gofastogt.UtcTimeMsec `json:"timestamp"`
	LastUpdate       gofastogt.UtcTimeMsec `json:"last_update"`
	OnlineClients    int64                 `json:"online_clients"`
	RequestsCount    int64                 `json:"requests_count"`
	ConnectionsCount int64                 `json:"connections_count"`
}

type PingInfo struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

type InputStreamStatisticInfo struct {
	Id             int                   `json:"id"`
	LastUpdateTime gofastogt.UtcTimeMsec `json:"last_update_time"`
	PrevTotalBytes int64                 `json:"prev_total_bytes"`
	TotalBytes     int64                 `json:"total_bytes"`
	Bps            int32                 `json:"bps"`
	Dbps           string                `json:"dbps"`
}

type OutputStreamStatisticInfo struct {
	Id             int                   `json:"id"`
	LastUpdateTime gofastogt.UtcTimeMsec `json:"last_update_time"`
	PrevTotalBytes int64                 `json:"prev_total_bytes"`
	TotalBytes     int64                 `json:"total_bytes"`
	Bps            int32                 `json:"bps"`
	Dbps           string                `json:"dbps"`
}

func GetStoreBytes(stats []Machine, startTimeStamp gofastogt.UtcTimeMsec) (*float64, error) {
	bytes := 0.0
	if len(stats) == 0 {
		return &bytes, nil
	}

	index := sort.Search(len(stats), func(i int) bool { return stats[i].Timestamp >= startTimeStamp })

	if index != len(stats) {
		for i := index; i < len(stats); i++ {
			bytes += float64(stats[i].HddTotal - stats[i].HddFree)
		}
		bytes /= float64(len(stats) - index)
	}

	return &bytes, nil
}

func GetNetBytes(stats []Machine, startTimeStamp gofastogt.UtcTimeMsec) (*float64, error) {
	bytes := 0.0
	if len(stats) < 2 {
		return &bytes, nil
	}

	index := sort.Search(len(stats), func(i int) bool { return stats[i].Timestamp >= startTimeStamp })

	if index != len(stats) {
		first := stats[index]
		last := stats[len(stats)-1]
		bytes = float64(last.TotalBytesOut - first.TotalBytesOut)
	}

	return &bytes, nil
}
