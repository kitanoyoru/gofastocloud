module gitlab.com/fastogt/gofastocloud

go 1.19

require (
	gitlab.com/fastogt/gofastocloud_base v1.10.20
	gitlab.com/fastogt/gofastogt v1.7.19
)

require golang.org/x/sys v0.9.0 // indirect
